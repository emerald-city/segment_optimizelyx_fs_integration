Demonstration of the integration of optimizelyx and segment

Setup:

- cd project_folder
- python3 -m venv venv
- pip install -r requirements.txt
- flask run
- Navigate to http://localhost:5000 on your browser.
