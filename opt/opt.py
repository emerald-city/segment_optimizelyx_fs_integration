import json
import requests
from flask import request
from optimizely import optimizely

def flatten_hook(obj):
    for key, value in obj.items():
        if isinstance(value, str):
            try:
                obj[key] = json.loads(value, object_hook=flatten_hook)
            except ValueError:
                pass
    return obj

OPTIMIZELY_APPLICATION_ID = '10162411897'


class Optimizely():

    def __init__(self):
        self.__project_id = OPTIMIZELY_APPLICATION_ID
        self.__client = None
        self.__config = None

    def get_config(self, force_sync=False):
        if not self.__config or force_sync:
            self.__config = self.__fetch_data_file()
        return self.__config

    def get_json_config(self):
        try:
            return json.loads(self.get_config(), object_hook=flatten_hook)
        except Exception as e:  # pylint: disable=broad-except, unused-variable
            return {'experiments': {}}

    def __fetch_data_file(self):
        return requests.get(self.__prepare_data_file_url()).text

    def __prepare_data_file_url(self):
        url = 'https://cdn.optimizely.com/json/{0}.json'
        return url.format(self.__project_id)

    def get_client(self, force_sync=False):
        if not self.__client or force_sync:
            self.__client = self.__create_client(force_sync)
        return self.__client

    def get_active_experiments(self):
        experiments = self.get_json_config()['experiments']
        return [x for x in experiments if x['status'] == 'Running']

    def get_active_variations(self, user_id: str):
        experiments = self.get_active_experiments()
        variations = {}
        for x in experiments:
            variations[x['key']] = self.get_variation(x['key'], user_id)
        return {'variations': variations, 'user_attributes': get_user_attributes(user_id)}

    def __create_client(self, force_sync=False):
        return optimizely.Optimizely(self.get_config(force_sync))

    def activate_experiment(self, experiment: str, user_id: str):
        attributes = get_user_attributes(user_id)
        return self.get_client().activate(experiment, user_id, attributes)

    def get_variation(self, experiment: str, user_id: str):
        attributes = get_user_attributes(user_id)
        return self.get_client().get_variation(experiment, user_id, attributes)

    def track_event(self, event: str, user_id: str):
        attributes = get_user_attributes(user_id)
        self.get_client().track(event, user_id, attributes)

    def recreate_client(self):
        self.get_client(True)

def get_user_attributes(user_id: str):
    return {
        'platform': get_platform()
    }

def get_request_origin():
    return request.headers.get('X-Forwarded-For', request.remote_addr)

def get_platform():
    return request.headers.get('User-Agent')
