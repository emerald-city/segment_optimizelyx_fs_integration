from flask import Flask, render_template
from opt import ab_tester

app = Flask(__name__)


@app.route('/')
def index():
    variation = ab_tester.activate_experiment('search_suggestions', 'foobarbaz')
    return render_template('index.html', variation=variation)
